from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='MLOps cource project by 23reg team.',
    author='MLOps-23reg-team',
    license='MIT',
)
