# local package
-e .
# external requirements
black==22.3.0
flake8==4.0.1
...
